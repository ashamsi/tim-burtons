package com.timburtons.timburtons.products;

import com.timburtons.timburtons.data.ApiCalls;
import com.timburtons.timburtons.data.ProductContentImpl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public final class ProductsPresenter implements ProductsContract.Presenter {

    @NonNull private final ProductsContract.View mView;
    @Nullable private Call<List<ProductContentImpl>> mCall;

    public ProductsPresenter(@NonNull final ProductsContract.View view) {
        mView = view;
        mView.setPresenter(this);
    }

    @UiThread
    @Override
    public final void getProducts() {
        mCall = ApiCalls.getProductContentListCall();
        if (mCall == null) {
            return;
        }

        mCall.enqueue(new Callback<List<ProductContentImpl>>() {
            @UiThread
            @Override
            public final void onResponse(@NonNull final Call<List<ProductContentImpl>> call, @NonNull final Response<List<ProductContentImpl>> response) {
                final List<ProductContentImpl> productList = response.body();
                if (response.isSuccessful() && productList != null) {
                    mView.showProducts(productList);
                } else {
                    mView.showErrorProductLoading();
                }
            }

            @UiThread
            @Override
            public final void onFailure(@NonNull final Call<List<ProductContentImpl>> call, @NonNull final Throwable t) {
                mView.showErrorProductLoading();
            }
        });
    }
}
