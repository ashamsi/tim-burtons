package com.timburtons.timburtons.products;

import com.timburtons.timburtons.R;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public class ProductsActivity extends AppCompatActivity {
    @Nullable private ProductsPresenter mPresenter;
    @Nullable private DrawerLayout mDrawerLayout;
    @Nullable private Spinner mSpinner;

    @UiThread
    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        // Set up the toolbar.
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.toolbar_products_title);
        }

        // Set up the navigation drawer.
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);
        final NavigationView navigationView = findViewById(R.id.navigation_view);
        if (navigationView != null) {
            setupDrawer(navigationView);
        }
    }

    @UiThread
    @Override
    public final boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Open the navigation drawer when the home icon is selected from the toolbar.
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @UiThread
    private void setupDrawer(@NonNull final NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.home_navigation_menu_item:
                                NavUtils.navigateUpFromSameTask(ProductsActivity.this);
                                break;
                            case R.id.products_navigation_menu_item:
                                // Do nothing, we're already on that screen
                                break;
                            default:
                                break;
                        }
                        // Close the navigation drawer when an item is selected.
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
        final View headerLayout = navigationView.getHeaderView(0);
        mSpinner = headerLayout.findViewById(R.id.nav_header_spinner);
        List<String> list = new ArrayList<String>();
        list.add("list 1");
        list.add("list 2");
        list.add("list 3");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(dataAdapter);

        // create fragment
        final ProductsFragment fragment = createFragment();

        // create presenter
        mPresenter = new ProductsPresenter(fragment);
    }

    private ProductsFragment createFragment() {
        ProductsFragment productsFragment = (ProductsFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (productsFragment == null) {
            // Create the fragment
            productsFragment = ProductsFragment.newInstance();
            addFragmentToActivity(getSupportFragmentManager(), R.id.contentFrame, productsFragment);
        }
        return productsFragment;
    }

    private void addFragmentToActivity(@NonNull final FragmentManager fragmentManager, final int frameId, @NonNull Fragment fragment) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commit();
    }
}
