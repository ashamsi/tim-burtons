package com.timburtons.timburtons.products;

import com.timburtons.timburtons.BaseView;
import com.timburtons.timburtons.data.ProductContentImpl;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public interface ProductsContract {
    interface Presenter {
        void getProducts();
    }

    interface View extends BaseView<Presenter> {
        void showProducts(@NonNull final List<ProductContentImpl> productList);
        void showErrorProductLoading();
        void showProgress();
    }
}
