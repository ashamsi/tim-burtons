package com.timburtons.timburtons.products;

import com.timburtons.timburtons.R;
import com.timburtons.timburtons.data.ProductContentImpl;
import com.timburtons.timburtons.products.ProductsContract.Presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-02-27.
 */
public final class ProductsFragment extends Fragment implements ProductsContract.View {
    @Nullable private ProductsContract.Presenter mPresenter;
    @Nullable private ProductContentAdapter mAdapter;
    @Nullable private RecyclerView mRecyclerView;
    @Nullable private ProgressBar mProgressBar;

    public static ProductsFragment newInstance() {
        ProductsFragment fragment = new ProductsFragment();
        final Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings ("ConstantConditions")
    @UiThread
    @Nullable
    @Override
    public final View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_products, container, false);
        mProgressBar = view.findViewById(R.id.pb_products);
        mRecyclerView = view.findViewById(R.id.rv_products);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        mAdapter = new ProductContentAdapter();
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    @UiThread
    @Override
    public final void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.getProducts();
        }
    }

    @UiThread
    @Override
    public final void showProducts(@NonNull final List<ProductContentImpl> dataList) {
        if (mProgressBar != null && mRecyclerView != null) {
            mProgressBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            if (mAdapter != null) {
                mAdapter.setDataList(dataList);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @UiThread
    @Override
    public final void showErrorProductLoading() {
        if (mProgressBar != null & mRecyclerView != null) {
            mProgressBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
        }
        if (getView() != null) {
            Snackbar.make(getView(), getString(R.string.error_loading_products), Snackbar.LENGTH_SHORT).show();
        }
    }

    @UiThread
    @Override
    public final void showProgress() {
        if (mProgressBar != null & mRecyclerView != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @UiThread
    @Override
    public final void setPresenter(@NonNull final Presenter presenter) {
        mPresenter = presenter;
    }
}
