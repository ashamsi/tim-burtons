package com.timburtons.timburtons.products;

import android.support.annotation.IntRange;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public interface ProductContent {

    int getId();
    String getName();
    String getSize();
    String getCost();
    String getType();
}
