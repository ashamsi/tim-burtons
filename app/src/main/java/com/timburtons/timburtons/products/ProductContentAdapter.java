package com.timburtons.timburtons.products;

import com.timburtons.timburtons.R;
import com.timburtons.timburtons.data.ProductContentImpl;
import com.timburtons.timburtons.products.ProductContentAdapter.ProductContentViewHolder;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-02-28.
 */
public final class ProductContentAdapter extends RecyclerView.Adapter<ProductContentViewHolder> {
    @Nullable private List<ProductContentImpl> mDataList;

    public final void setDataList(@NonNull final List<ProductContentImpl> dataList) {
        mDataList = dataList;
    }

    @UiThread
    @Override
    public final ProductContentViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_content_adapter_item, parent, false);
        return new ProductContentViewHolder(view);
    }

    @UiThread
    @Override
    public final void onBindViewHolder(final ProductContentViewHolder holder, final int position) {
        final TextView productDescription = holder.mProductDescription;
        final TextView productCost = holder.mProductCost;

        final ProductContentImpl productContent = mDataList.get(position);
        final String size = productContent.getSize();
        final String description = productContent.getName() + (size != null ? " " + size : "");

        productDescription.setText(description);
        final String cost = "$" + productContent.getCost();
        productCost.setText(cost);
    }

    @Override
    public int getItemCount() {
        return mDataList != null ? mDataList.size() : 0;
    }

    static class ProductContentViewHolder extends RecyclerView.ViewHolder {
        @NonNull private TextView mProductDescription;
        @NonNull private TextView mProductCost;

        public ProductContentViewHolder(@NonNull final View itemView) {
            super(itemView);
            mProductDescription = itemView.findViewById(R.id.txt_product_description);
            mProductCost = itemView.findViewById(R.id.txt_product_cost);
        }
    }
}
