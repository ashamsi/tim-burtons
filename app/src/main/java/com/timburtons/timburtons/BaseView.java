package com.timburtons.timburtons;

import android.support.annotation.UiThread;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public interface BaseView<T> {
    /**
     * Sets provided {@link T} presenter.
     * @param presenter the {@link T} presenter to be set.
     */
    @UiThread
    void setPresenter(T presenter);
}
