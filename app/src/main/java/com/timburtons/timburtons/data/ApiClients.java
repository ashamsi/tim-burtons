package com.timburtons.timburtons.data;

import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public class ApiClients {
    private static Retrofit sRetrofit;

    static synchronized Retrofit getApiClient() {
        if (sRetrofit == null) {
            sRetrofit = new Retrofit
                    .Builder()
                    .baseUrl("http://192.168.1.113/")
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build();
        }
        return sRetrofit;
    }
}

