package com.timburtons.timburtons.data;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public class ApiServices {
    private static ApiInterface sApiInterface;

    static synchronized ApiInterface getApiInterface() {
        if (sApiInterface == null) {
            sApiInterface = ApiClients
                    .getApiClient()
                    .create(ApiInterface.class);
        }
        return sApiInterface;
    }
}
