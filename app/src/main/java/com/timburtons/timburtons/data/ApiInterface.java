package com.timburtons.timburtons.data;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public interface ApiInterface {

    @GET ("/test/products.json")
    Call<List<ProductContentImpl>> getProducts();
}
