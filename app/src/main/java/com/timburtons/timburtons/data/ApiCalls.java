package com.timburtons.timburtons.data;

import java.util.List;

import retrofit2.Call;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public class ApiCalls {
    public static Call<List<ProductContentImpl>> getProductContentListCall() {
        return ApiServices
                .getApiInterface()
                .getProducts();
    }
}
