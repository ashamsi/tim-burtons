package com.timburtons.timburtons.data;

import com.timburtons.timburtons.products.ProductContent;

import android.support.annotation.IntRange;
import android.support.annotation.Nullable;

/**
 * TODO: Javadoc.
 * <p>
 * Created by Artur Shamsi on 2018-03-03.
 */
public class ProductContentImpl implements ProductContent {
    @IntRange (from = 0) private int id;
    @Nullable private String name;
    @Nullable private String size;
    @Nullable private String cost;
    @Nullable private String type;

    @IntRange (from = 0)
    @Override
    public final int getId() {
        return id;
    }

    @Nullable
    @Override
    public final String getName() {
        return name;
    }

    @Nullable
    @Override
    public String getSize() {
        return size;
    }

    @Nullable
    @Override
    public final String getCost() {
        return cost;
    }

    @Nullable
    @Override
    public final String getType() {
        return type;
    }
}
